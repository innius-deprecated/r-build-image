#!/bin/bash
MY_DIR=$(dirname $(readlink -f $0))
. $MY_DIR/util.sh

if [ -z "$1" ]
then
  echo "Usage: `basename $0` [service-name]"
  exit $E_NOARGS
fi
SERVICE_NAME=$1

verify_env_var "BUILD_NUMBER"
verify_env_var "BRANCH"
sanitize_docker_branch "BRANCH"

IMAGE_NAME="quay.io/toincrease/${SERVICE_NAME}"

docker build --pull -t $IMAGE_NAME .
docker tag $IMAGE_NAME:latest $IMAGE_NAME:$BRANCH.$BUILD_NUMBER
docker push $IMAGE_NAME:latest
docker push $IMAGE_NAME:$BRANCH.$BUILD_NUMBER
