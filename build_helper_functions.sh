#!/bin/bash
export SHIPPABLE_REDIS_PORT=6379;
export SHIPPABLE_REDIS_BINARY="/usr/bin/redis-server";
export SHIPPABLE_REDIS_CMD="$SHIPPABLE_REDIS_BINARY";

function start_dynamodb {
    echo "Starting dynamodb as a detached background process..."
    java -Djava.library.path=/tmp/DynamoDBLocal_lib -jar /opt/dynamodb-local/DynamoDBLocal.jar -inMemory &
    sleep 2
}

function start_mountebank {
    echo "Starting mountebank as a detached background process..."
    mb &
    sleep 2
}

function start_redis {
    echo "Starting redis service" 
    start_generic_service "redis" "$SHIPPABLE_REDIS_BINARY" "$SHIPPABLE_REDIS_CMD" "$SHIPPABLE_REDIS_PORT";
}

source /u14pls/test/function_start_generic.sh