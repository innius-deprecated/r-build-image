#!/bin/bash
IMAGE=$(cat /tmp/version.txt)
IPADDRESS=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

# pull the new image
SLEEP_SECONDS=5
IMAGE=$(cat /tmp/version.txt)

docker_image_available() {
    IMAGE_AVAILABLE=$(docker pull ${IMAGE})

    if [[ "$?" != "0" ]]; then
        echo "image not yet found"
        return 1
    fi

    return 0
}

while ! docker_image_available; do
    echo "Docker image not available yet, retrying in ${SLEEP_SECONDS} seconds"
    sleep ${SLEEP_SECONDS}
done

# stop the container
docker inspect ${APPLICATION_NAME}
if [[ "$?" == "0" ]]; then
  docker stop $APPLICATION_NAME
  docker rm $APPLICATION_NAME
fi

# start the new container
docker run -d --restart=always -p $IPADDRESS:8080:8080 --name $APPLICATION_NAME --log-driver=syslog  --env-file="/etc/sysconfig/service_environment" $IMAGE

