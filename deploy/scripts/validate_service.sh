IPADDRESS=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)

NEXT_WAIT_TIME=0
until curl -L --fail --silent $IPADDRESS:8080/health || [ $NEXT_WAIT_TIME -eq 4 ]; do    
    sleep $(( NEXT_WAIT_TIME++ ))
done
