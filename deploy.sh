#!/bin/bash
MY_DIR=$(dirname $(readlink -f $0))
. $MY_DIR/util.sh

if [ -z "$1" ]
then
  echo "Usage: `basename $0` [service-name]"
  exit $E_NOARGS
fi
SERVICE_NAME=$1

verify_env_var "BRANCH"
verify_env_var "BUILD_NUMBER"

sanitize_docker_branch "BRANCH"

IMAGE_NAME=quay.io/toincrease/$SERVICE_NAME:$BRANCH.${BUILD_NUMBER}
BUCKET_NAME=$SERVICE_NAME
if [ "$BRANCH" != "master" ]; then BUCKET_NAME=${BUCKET_NAME}"_"$BRANCH ; fi

BUCKET_NAME=s3://microservices.to-increase.io-us-east-1/codedeploy/${BUCKET_NAME}.zip
echo $IMAGE_NAME > /deploy/version.txt

# Copy the template & config file:
cp infrastructure/template.json /deploy/template.json
cp infrastructure/config.json /deploy/config.json

pushd /deploy
zip package.zip . -r
aws s3 cp package.zip $BUCKET_NAME --region us-east-1
echo "Build S3 Artifact version: "
aws s3api head-object --bucket microservices.to-increase.io-us-east-1 --key codedeploy/${SERVICE_NAME}.zip --region us-east-1
popd
