FROM r-base:3.2.3

RUN apt-get update && apt-get install -y \
    curl \
    wget \
    awscli \
  && rm -rf /var/lib/apt/lists/*

RUN wget http://ftp.de.debian.org/debian/pool/main/g/golang-1.7/golang-1.7-go_1.7.3-1_amd64.deb -O golang-go.deb && \
    wget http://ftp.de.debian.org/debian/pool/main/g/golang-1.7/golang-1.7-src_1.7.3-1_amd64.deb -O golang-src.deb && \
    dpkg -i *.deb && \
    rm *.deb
    
RUN R -e 'options(repos = c(CRAN = "http://cran.rstudio.com/")); install.packages(c("Rserve","DBI", "RPostgreSQL", "stringr", "jsonlite", "R6", "testthat"))'
    
# Add a simple initialization wrapper because shippable mangles the whole environment:
RUN echo "#!/bin/bash" >> /init
RUN chmod +x /init

ADD deploy/ /deploy
ADD deploy.sh /
ADD util.sh /
ADD docker_build.sh /
ADD build_helper_functions.sh /
