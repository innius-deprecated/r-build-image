SUCCESS=0
E_NOARGS=65

function verify_env_var {
		# Trick to get the var from a string
		eval "VAR=\${${1}}"
		if [ -z "${VAR}" ]
		then
				echo "An env var for '${1}' is mandatory, cannot continue"
				exit 1
		fi
}

function sanitize_docker_branch {
		eval "VAR=\${${1}}"
		# Strip / and :
		r="${VAR///}"
		r="${r//:}"
		echo $r
		eval "$1=$r"
}
